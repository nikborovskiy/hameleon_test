<?php

/**
 * This is the model class for table "{{books}}".
 *
 * The followings are the available columns in table '{{books}}':
 * @property integer $id
 * @property string $name
 * @property string $date_create
 * @property string $date_update
 * @property string $preview
 * @property string $date
 * @property integer $author_id
 *
 * The followings are the available model relations:
 * @property Authors $author
 */
class Books extends CActiveRecord
{
    // путь для сохранения изображений книги
    const IMAGE_ITEM_PATH = 'public/img/book_img/';

    public $imageFile;

    public $date_start, $date_end;

    public function behaviors()
    {
        return array(
            'CTimestampBehavior' => array(
                'class' => 'zii.behaviors.CTimestampBehavior',
                'setUpdateOnCreate' => false,
                'createAttribute' => 'date_create',
                'updateAttribute' => 'date_update',
            ),
        );
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{books}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return array(
            array('date_update, author_id', 'required'),
            array('author_id', 'numerical', 'integerOnly' => true),
            array('name, preview', 'length', 'max' => 255),
            array('date_create, date, date_start, date_end', 'safe'),
            array('id, name, date_create, date_update, preview, date, author_id, date_start, date_end', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'author' => array(self::BELONGS_TO, 'Authors', 'author_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'name' => 'Название книги',
            'date_create' => 'Дата добавления',
            'date_update' => 'Дата обновления записи',
            'preview' => 'Превью книги',
            'date' => 'Дата выхода книги',
            'author_id' => 'Автор',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        $criteria = new CDbCriteria;
        $criteria->alias = 't';
        $criteria->with = ['author'];

        $criteria->compare('id', $this->id);
        $criteria->compare('name', $this->name, true);
        $criteria->compare('date_create', $this->date_create, true);
        $criteria->compare('date_update', $this->date_update, true);
        $criteria->compare('preview', $this->preview, true);
        if (!empty($this->date_start)) {
            $criteria->compare('date', '>=' . date('Y-m-d', strtotime($this->date_start)));
        }
        if (!empty($this->date_end)) {
            $criteria->compare('date', '<=' . date('Y-m-d', strtotime($this->date_end)));
        }
        $criteria->compare('author_id', $this->author_id);

        $sort = new CSort();
        $sort->attributes = [
            'author_id' => [
                'asc' => 'author.firstname, author.lastname',
                'desc' => 'author.firstname DESC, author.lastname DESC',
            ],
            'name',
            'date',
            'date_create'
        ];

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'sort' => $sort
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Books the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * Получение пути для изображений товара
     * @return string
     */
    public static function getImagePath()
    {
        return Yii::getPathOfAlias('webroot') . DIRECTORY_SEPARATOR . self::IMAGE_ITEM_PATH;
    }

    /**
     * Получение URL изображения
     * @return string
     */
    public function getImageUrl()
    {
        return Yii::app()->getBaseUrl(true) . DIRECTORY_SEPARATOR . self::IMAGE_ITEM_PATH . ($this->preview ?: 'default.png');
    }

    /**
     * Сохранение изображения и удаление старого
     * @throws Exception
     */
    public function saveImage()
    {
        if ($this->imageFile instanceof CUploadedFile) {
            if ($this->preview) {
                unlink($this->getImagePath() . $this->preview);
            }
            $name = $this->imageFile->getName();
            $this->imageFile->saveAs($this->getImagePath() . $name);
            $this->preview = $name;
            $this->update('preview');
        }
    }

    /**
     * Список авторов для выпадающего списка
     * @return array
     */
    public function getAuthorListData()
    {
        return CHtml::listData(Authors::model()->findAll(), 'id', function ($data) {
            return $data->firstname . ' ' . $data->lastname;
        });
    }
}
