<?php

/**
 * Class BookForm
 */
class BookForm extends CFormModel
{
    public $id;
    public $name;
    public $date_create;
    public $date_update;
    public $imageFile;
    public $date;
    public $author_id;

    private $_model = null;

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return array(
            array('name', 'required'),
            array('author_id', 'numerical', 'integerOnly' => true),
            array('name', 'length', 'max' => 255),
            array('imageFile', 'file', 'types' => 'jpg, gif, png', 'allowEmpty' => true),
            array('date_create, date', 'safe'),
        );
    }

    /**
     * @return null|Books
     */
    public function getModel()
    {
        if ($this->_model === null && $this->id) {
            $this->_model = Books::model()->findByPk($this->id);
        }
        return $this->_model;
    }

    /**
     * @param $data
     * @param string $formName
     * @return bool
     */
    public function load($data, $formName = 'BookForm')
    {
        $model = $this->getModel();
        if (!$model->isNewRecord) {
            $this->setAttributes($model->getAttributes(), false);
        }
        if (isset($data[$formName])) {
            $this->setAttributes($data[$formName]);
            return true;
        }
        return false;
    }

    /**
     * Save form
     *
     * @return bool
     */
    public function save()
    {
        if ($this->validate()) {
            $model = $this->getModel();
            if ($model) {
                $model->setAttributes($this->getAttributes(), false);
                if ($model->save()) {
                    $model->imageFile = CUploadedFile::getInstance($this, 'imageFile');
                    $model->saveImage();
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'name' => 'Название книги',
            'date_create' => 'Дата обновления записи',
            'date_update' => 'Дата создания записи',
            'imageFile' => 'Превью книги',
            'date' => 'Дата выхода книги',
            'author_id' => 'Автор',
        );
    }
}
