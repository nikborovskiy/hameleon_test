<?php
/**
 * @var BooksController $this
 * @var Books $model
 * @var CActiveForm $form
 */

Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/public/js/custom.js', CClientScript::POS_END);
?>
<div class="form filter">
    <?php $form = $this->beginWidget('CActiveForm', array(
        'method' => 'get',
        'id' => 'book-index-filter-form',
        'enableClientValidation' => true,
        'clientOptions' => array(
            'validateOnSubmit' => true,
        ),
    )); ?>

    <div class="row">
        <?php echo $form->dropDownList($model, 'author_id', $model->getAuthorListData(), ['prompt' => 'автор']); ?>
        <?php echo $form->textField($model, 'name', ['placeholder' => 'название книги']); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model, 'date', ['class' => 'date-range-label']); ?>
        <?php echo $form->dateField($model, 'date_start'); ?> до
        <?php echo $form->dateField($model, 'date_end'); ?>
        <?php echo CHtml::submitButton('искать', ['class' => 'filter-submit-button']); ?>
        <span class="clearfix"></span>
    </div>

    <?php $this->endWidget(); ?>
</div>

<?php $this->widget('zii.widgets.grid.CGridView', array(
    'dataProvider' => $model->search(),
    'summaryText' => false,
    'ajaxUpdate' => false,
    'rowHtmlOptionsExpression' => 'array("data-id"=>$data->id)',
    'htmlOptions' => [
        'class' => 'books-grid grid-view'
    ],
    'columns' => array(
        'id',
        'name',
        [
            'name' => 'preview',
            'value' => function ($data) {
                /** @var Books $data */
                return CHtml::link(
                    CHtml::image($data->getImageUrl(), 'Обложка книги', ['class' => 'preview-img']),
                    $data->getImageUrl(),
                    ['class' => 'fancy-target']
                );
            },
            'htmlOptions' => [
                'class' => 'img-cell'
            ],
            'type' => 'html'
        ],
        [
            'name' => 'author_id',
            'value' => function ($data) {
                /** @var Books $data */
                return $data->author->firstname . ' ' . $data->author->lastname;
            }
        ],
        [
            'name' => 'date',
            'value' => function ($data) {
                /** @var Books $data */
                return Yii::app()->dateFormatter->format('d MMM y', strtotime($data->date));
            }
        ],
        [
            'name' => 'date_create',
            'value' => function ($data) {
                /** @var Books $data */
                return Yii::app()->dateFormatter->format('d MMM y', strtotime($data->date_create));
            }
        ],
        array(
            'class' => 'CButtonColumn',
            'header' => 'Кнопки действий',
            'template' => '{update} {view} {delete}',
            'buttons' => [
                'update' => [
                    'label' => '[ред]',
                    'imageUrl' => false,
                    'options' => ['target' => '_blank'],
                    'visible' => function ($row, $data) {
                        return !Yii::app()->user->isGuest;
                    },
                ],
                'view' => [
                    'label' => '[просм]',
                    'imageUrl' => false,
                    'url' => 'javascript://',
                    'options' => [
                        'class' => 'js-view-button',
                        'id' => "$data->id"
                    ],
                ],
                'delete' => [
                    'label' => '[удл]',
                    'imageUrl' => false,
                    'visible' => function ($row, $data) {
                        return !Yii::app()->user->isGuest;
                    },
                ]
            ]
        ),
    ),
)); ?>

<?php $this->widget('application.extensions.fancybox.ALFancybox', [
        'targetDOM' => '.fancy-target',
        'asDialog' => true,
        'hideDOM' => '#comment-form',
        'helperButton' => true
    ]
); ?>

<div class="modal fade" id="book-view" tabindex="-1" role="dialog" aria-labelledby="book-view-label">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="book-view-label">Данные книги</h4>
            </div>
            <div class="modal-body">

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
            </div>
        </div>
    </div>
</div>
