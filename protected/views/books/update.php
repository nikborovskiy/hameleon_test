<?php
/**
 * @var BookForm $bookForm
 * @var CActiveForm $form
 */
?>
<fieldset>
    <legend><h2>Редактирование книги <?= $form->name ?></h2></legend>
    <?= CHtml::link('Вернуться к списку', Yii::app()->session['returnUrl'], ['class' => 'btn btn-primary']) ?>
    <div class="form">
        <?php $form = $this->beginWidget('CActiveForm', array(
            'id' => 'book-form',
            'htmlOptions' => [
                'enctype' => 'multipart/form-data',
            ],
            'enableClientValidation' => true,
            'clientOptions' => array(
                'validateOnSubmit' => true,
            ),
        )); ?>

        <div class="row">
            <?php echo $form->labelEx($bookForm, 'name'); ?>
            <?php echo $form->textField($bookForm, 'name'); ?>
            <?php echo $form->error($bookForm, 'name'); ?>
        </div>

        <div class="row">
            <?php echo $form->labelEx($bookForm, 'author_id'); ?>
            <?php echo $form->dropDownList($bookForm, 'author_id', $bookForm->getModel()->getAuthorListData()); ?>
            <?php echo $form->error($bookForm, 'author_id'); ?>
        </div>

        <div class="row">
            <?php echo $form->labelEx($bookForm, 'date'); ?>
            <?php echo $form->dateField($bookForm, 'date'); ?>
            <?php echo $form->error($bookForm, 'date'); ?>
        </div>

        <div class="row">
            <?php echo $form->labelEx($bookForm, 'imageFile'); ?>
            <?php echo $form->fileField($bookForm, 'imageFile'); ?>
            <?php echo $form->error($bookForm, 'imageFile'); ?>
            <?php if ($bookForm->getModel()->preview): ?>
                <div>
                    <?= CHtml::image($bookForm->getModel()->getImageUrl(), 'Обложка книги', ['height' => '150px']) ?>
                </div>
            <?php endif; ?>
        </div>

        <div class="row buttons">
            <?php echo CHtml::submitButton('Сохранить'); ?>
        </div>

        <?php $this->endWidget(); ?>
    </div>
</fieldset>
