<?php
/**
 * @var Books $model
 */
?>

<?php $this->widget('zii.widgets.CDetailView', array(
    'data' => $model,
    'attributes' => array(
        'id',
        'name',
        'date_create',
        'date_update',
        'date',
        [
            'label' => 'Автор',
            'value' => $model->author->firstname . ' ' . $model->author->lastname
        ],
        [
            'label' => 'Превью',
            'type' => 'html',
            'value' => CHtml::image($model->getImageUrl(), 'Book preview', ['height' => '150px'])
        ],
    ),
)); ?>