<?php

class m170704_123719_create_table_authors extends CDbMigration
{
    public function up()
    {
        $this->createTable('{{authors}}', [
            'id' => 'pk',
            'firstname' => 'VARCHAR(255) DEFAULT NULL COMMENT "Имя автора"',
            'lastname' => 'VARCHAR(255) DEFAULT NULL COMMENT "Фамилия автора"',
        ]);

        $data = [
            [
                'firstname' => 'Федор',
                'lastname' => 'Достоевский',
            ],
            [
                'firstname' => 'Лев',
                'lastname' => 'Толстой',
            ],
            [
                'firstname' => 'Александр',
                'lastname' => 'Пушкин',
            ],
        ];

        for ($i = 0; $i < count($data); $i++) {
            $id = $i + 1;
            $this->insert('{{authors}}', [
                'id' => $id,
                'firstname' => $data[$i]['firstname'],
                'lastname' => $data[$i]['lastname'],
            ]);
        }
    }

    public function down()
    {
        $this->dropTable('{{authors}}');
    }

    /*
    // Use safeUp/safeDown to do migration with transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}