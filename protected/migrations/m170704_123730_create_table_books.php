<?php

class m170704_123730_create_table_books extends CDbMigration
{
    public function up()
    {
        $this->createTable('{{books}}', [
            'id' => 'pk',
            'name' => 'VARCHAR(255) DEFAULT NULL COMMENT "Название книги"',
            'date_create' => 'DATETIME COMMENT "Дата обновления записи"',
            'date_update' => 'TIMESTAMP COMMENT "Дата создания записи"',
            'preview' => 'VARCHAR(255) DEFAULT NULL COMMENT "Превью книги"',
            'date' => 'DATE COMMENT "Дата выхода книги"',
            'author_id' => 'INT(11) NOT NULL COMMENT "Автор"',
        ]);

        $this->addForeignKey('book_has_author', '{{books}}', 'author_id', '{{authors}}', 'id', 'CASCADE', 'CASCADE');

        $data = [
            [
                'name' => 'Идиот',
                'author_id' => 1,
            ],
            [
                'name' => 'Война и мир',
                'author_id' => 2,
            ],
            [
                'name' => 'Руслан и Людмила',
                'author_id' => 3,
            ],
            [
                'name' => 'Медный всадник',
                'author_id' => 3,
            ],
            [
                'name' => 'Домик в Коломне',
                'author_id' => 3,
            ],
        ];

        $datetime = date('Y-m-d H:i:s');
        $date = date('Y-m-d');
        echo $datetime;
        for ($i = 0; $i < count($data); $i++) {
            $id = $i + 1;
            $this->insert('{{books}}', [
                'id' => $id,
                'name' => $data[$i]['name'],
                'date_create' => $datetime,
                'date' => $date,
                'author_id' => $data[$i]['author_id'],
            ]);
        }
    }

    public function down()
    {
        $this->dropForeignKey('book_has_author', '{{books}}');
        $this->dropTable('{{books}}');
    }

    /*
    // Use safeUp/safeDown to do migration with transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}