<?php

class BooksController extends Controller
{
    /**
     * Правила доступа
     * @return array
     */
    public function accessRules()
    {
        return array(
            array('deny',
                'actions' => array('delete', 'update'),
                'users' => array('?'),
            ),
            array('allow',
                'actions' => array('delete', 'update'),
                'users' => array('@'),
            ),
        );
    }

    /**
     * Список книг
     */
    public function actionIndex()
    {
        $model = new Books();
        if (isset($_GET['Books'])) {
            $model->setAttributes($_GET['Books']);
        }
        $this->render('index', ['model' => $model]);
    }

    /**
     * Редактирование книги
     * @param $id
     */
    public function actionUpdate($id)
    {
        $form = new BookForm();
        $form->id = $id;
        if ($form->load($_POST) && $form->save()) {
            $returnUrl = Yii::app()->session['returnUrl'];
            unset(Yii::app()->session['returnUrl']);
            $this->redirect($returnUrl);
        } else if ($form->getModel()) {
            if (!isset(Yii::app()->session['returnUrl']) || empty(Yii::app()->session['returnUrl'])) {
                Yii::app()->session['returnUrl'] = Yii::app()->request->urlReferrer;
            }
            $this->render('update', ['bookForm' => $form]);
        }
        $this->redirect(Yii::app()->session['returnUrl'] ?: '/');
    }

    /**
     * Удаление книги
     * @param $id
     */
    public function actionDelete($id)
    {
        Books::model()->deleteByPk($id);
        $this->redirect(Yii::app()->request->urlReferrer);
    }

    /**
     * Получение данных для просмотра данных о книге в модальном окне.
     * @param $id
     * @throws CException
     * @throws CHttpException
     */
    public function actionView($id)
    {
        if (Yii::app()->request->isAjaxRequest) {
            $result['status'] = 2;
            if ($model = Books::model()->findByPk($id)) {
                $result = [
                    'status' => 1,
                    'html' => $this->renderPartial('_partial/_book_view', ['model' => $model], true, false)
                ];
            }
            echo json_encode($result);
        } else {
            throw new CHttpException(404, 'Страница не найдена');
        }
    }
}