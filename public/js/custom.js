$('.js-view-button').on('click', function (e) {
    e.preventDefault();
    var $elm = $(this);
    var id = $elm.closest('tr').data('id');
    $.get('/books/view', {id: id}, function (data) {
        if ('status' in data && data.status == 1) {
            var $modal = $('#book-view');
            $modal.find('.modal-body').html(data.html);
            $modal.modal();
        }
    }, 'json');
});